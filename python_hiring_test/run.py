import pandas as pd
import numpy as np


# on base plus slugging = on base percentage + slugging percentage
def ops(file):
    slg_stat = file['TB'] / file['AB']
    obp_stat_num = file['H'] + file['BB'] + file['HBP']
    obp_stat_den = file['AB'] + file['BB'] + file['SF'] + file['HBP']
    obp_stat = obp_stat_num / obp_stat_den
    ops_stat = slg_stat + obp_stat
    return round(ops_stat, 3)


# slugging percentage = total bases + at bats
def slg(file):
    slg_stat = (file['TB'] / file['AB'])
    return round(slg_stat, 3)


# on base percentage = (hits + walks + hit by pitch) / plate apperiences
def obp(file):
    obp_stat_num = file['H'] + file['BB'] + file['HBP']
    obp_stat_den = file['AB'] + file['BB'] + file['SF'] + file['HBP']
    obp_stat = obp_stat_num / obp_stat_den
    return round(obp_stat, 3)


# batting average = hits / at bats
def avg(file):
    avg_stat = (file['H']) / file['AB']
    return round(avg_stat, 3)


# fuction that filters more than 20 plate apperances
def plate_app(file):
    return file[file["PA"] >= 25]


# group appropriately by subject and split, sum up all values and calc stat
def combo_read(file, stat, group, split):
    file_group = file.groupby([group, split])
    file_stat = file_group.sum()
    file_filter = plate_app(file_stat)
    stat_val = stat(file_filter)
    return (round(stat_val, 3))


def main():
    # read and use appropriate columns
    cols = ['PitcherId', 'HitterId', "PitcherSide", 'HitterSide', 'PitcherTeamId',
            'HitterTeamId', 'PA', 'AB', 'H', '2B', '3B', 'HR', 'TB', 'BB', 'SF', 'HBP']
    file = pd.read_csv("./data/raw/pitchdata.csv", usecols=cols)
# initiate empty lists for output df
    out_subid, out_stat, out_split, out_subject, out_value = [], [], [], [], []

# itr over stats, subjects and splits
    stat_dict = {"avg": avg, "obp": obp, "slg": slg, "ops": ops}
    subject_list = ['HitterId', 'HitterTeamId', 'PitcherId', 'PitcherTeamId']
    split_list = ['PitcherSide', 'HitterSide']

    for subject in subject_list:
        for split in split_list:
            for key in stat_dict:
                # Make sure that we eliminate same subject and split
                if(subject[0] == split[0]):
                    pass
                else:
                    # retrieve statistic grouped by SUBJECT and SPLIT and calc each
                    # statistic in each category
                    write_stat = combo_read(file, stat_dict[key], subject, split)
                    # appending lists to prepare for writing dataframe
                    for iters in range(len(write_stat)):
                        out_subject.append(subject)
                        out_stat.append(key.upper())
                    unzip_subid, unzip_split = zip(*write_stat.keys())
                    vs = "vs "
                    if(split == "PitcherSide"):
                        side = "HP"
                        unzip_split = [vs + x + side for x in unzip_split]

                    else:
                        side = "HH"
                        unzip_split = [vs + x + side for x in unzip_split]

                    # appending keys and values
                    out_subid.append(unzip_subid)
                    out_split.append(unzip_split)
                    out_value.append(write_stat.values)

    # flattening out the arrays
    out_value = np.concatenate(out_value).ravel()
    out_subid = np.concatenate(out_subid).ravel()
    out_split = np.concatenate(out_split).ravel()

# create an output dictionary and convert to dataframe
    out_dict = {"Value": out_value, "Subject": out_subject, "Stat": out_stat,
                "Split": out_split, "SubjectId": out_subid}
    out_df = pd.DataFrame(out_dict)
    out_df = out_df[["SubjectId", "Stat", "Split", "Subject", "Value"]]

    out_df.sort_values(["SubjectId", "Stat", "Split", "Subject"], inplace=True)
    out_df.set_index("SubjectId", inplace=True)
    out_df.to_csv("./data/processed/output.csv")

if __name__ == '__main__':
    main()
