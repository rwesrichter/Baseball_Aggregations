Install: `pip install -r requirements.txt`

Run the test suite by opening a command-line, cd in to the repo, and running  
the following command: ```pytest -v```
